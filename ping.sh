#!/bin/bash
# ESTE SCRIPT HACE PING A LOS SITIOS DE INTERNET INDICADOS CADA X SEGUNDOS
# PARA DETERMINAR QUE HAYA ACCESO A INTERNET

# -------------------------------------------------------------------------------------------------
TARGETS="8.8.8.8 8.8.4.4"	
# -------------------------------------------------------------------------------------------------
MAXFAIL=1					# nº max. de repeticiones antes de apagar heartbeat
PAQUETES=5					# nº max. de paquetes a enviar para cada dominio
CONTROLFILE="/tmp/$(basename $0)__pingfailed"	# Archivo de reintentos
CMDPING=`fping -q -C $PAQUETES -s $TARGETS 2>&1>/dev/null | grep -E "alive|elapsed real time"`
# -------------------------------------------------------------------------------------------------

touch $CONTROLFILE

ISALIVE=`echo $CMDPING|grep -Eo ".*?alive"|awk '{ print $1 }'`
TOTAL=`echo $CMDPING|grep -Eo "alive.*?"`

if [ "$ISALIVE" == "0" -o "$ISALIVE" == "" ]; then
	if [ `cat $CONTROLFILE` == "$MAXFAIL" ]; then
		logger -t "$(basename $0)" "Internet Test (Error, failover a firewall ...)"
		/etc/init.d/heartbeat stop
		echo 0 > $CONTROLFILE
	else
		let nerror=`cat $CONTROLFILE`+1
		echo $nerror > $CONTROLFILE
		exit 0
	fi
else
	if [ "`pidof heartbeat`" != "" ]; then
		logger -t "$(basename $0)" "Internet Test (ping [$PAQUETES pkts]) $TOTAL"
	else
		logger -t "$(basename $0)" "conectividad ok. levantando hb......."
		/etc/init.d/heartbeat start
	
	fi

	echo 0 > $CONTROLFILE
fi
